package com.sgo.mdevcash.Beans;

public class News {

    //private variables
    int _id;
    String _name;
    String date;
    String link;
    String code;
    String seen;
    String msg;

    // Empty constructor
    public News(){

    }
    // constructor
    public News(int id, String name, String date,String link,String code,String seen){
        this._id = id;
        this._name = name;
        this.date = date;
        this.code=code;
        this.link=link;
        this.seen=seen;
    }

    // constructor
    public News(String name, String date,String link,String code){
        this._name = name;
        this.date = date;
        this.code=code;
        this.link=link;
 
    }
   
    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getName(){
        return this._name;
    }

    // setting name
    public void setName(String name){
        this._name = name;
    }

    // getting phone number
    public String getDate(){
        return this.date;
    }

    // setting phone number
    public void setDate(String date){
        this.date = date;
    }

    // getting phone number
    public String getLink(){
        return this.link;
    }

    // setting phone number
    public void setLink(String link){
        this.link = link;
    }


    // getting phone number
    public String getCode(){
        return this.code;
    }

    // setting phone number
    public void setCode(String code){
        this.code = code;
    }
    public String getSeen() {
        // TODO Auto-generated method stub
        return this.seen;
    }
    public void setSeen(String seen){
        this.seen=seen;
    }

}
