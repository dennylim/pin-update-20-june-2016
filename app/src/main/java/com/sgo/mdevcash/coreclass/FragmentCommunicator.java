package com.sgo.mdevcash.coreclass;/*
  Created by Administrator on 10/13/2014.
 */

import java.util.HashMap;

public interface FragmentCommunicator {
    public void respond(HashMap<String,String> data, Boolean flag);
}
