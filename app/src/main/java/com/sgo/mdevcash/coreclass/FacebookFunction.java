package com.sgo.mdevcash.coreclass;/*
  Created by Administrator on 10/1/2015.
 */

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.facebook.*;
import com.facebook.login.LoginManager;
import com.sgo.mdevcash.R;

public class FacebookFunction {

    private static FacebookFunction singleton = null;

    private FacebookFunction(){

        Log.d("Masukk FacebookFunction contructor","masuuukkk");

//        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);

        mCallBackManager = CallbackManager.Factory.create();
        mATT = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if(oldAccessToken != null)
                    Log.d("old access token", oldAccessToken.getToken() + " / " + oldAccessToken.getExpires().toString() + " / " + oldAccessToken.getUserId());
                if(currentAccessToken != null) {
                    Log.d("current access token", currentAccessToken.getToken() + " / " + currentAccessToken.getExpires().toString() + " / " + currentAccessToken.getUserId());
                    setFinalAT(currentAccessToken);
                }
                else
                    setFinalAT(null);
            }
        };

        mPT = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if(oldProfile != null)
                    Log.d("old profile", oldProfile.toString());
                if(currentProfile != null) {
                    Log.d("current profile", currentProfile.toString());
                    setFinalProf(currentProfile);
                }
                else
                    setFinalProf(null);
            }
        };
    }

    private static void createCallback(){
        Initialize().setmCallBackManager(CallbackManager.Factory.create());
    }

    public static FacebookFunction Initialize( ) {
        if(singleton == null) {
            singleton = new FacebookFunction();
        }
        return singleton;
    }

    public static FacebookFunction getInstance( ) {
        if(singleton != null) {
            createCallback();
            return singleton;
        }
        return Initialize();
    }

    private AccessToken FinalAT;
    private Profile FinalProf;

    private CallbackManager mCallBackManager;
    private AccessTokenTracker mATT;
    private ProfileTracker mPT;



    public Boolean isLogin(){
        return FinalAT != null && !FinalAT.isExpired() && !FinalAT.getToken().isEmpty();
    }

    public Boolean isHavePublishPermission() {
        return isLogin() && getFinalAT().getPermissions().contains("publish_actions");
    }


    public void Logout(){
        LoginManager.getInstance().logOut();
        setFinalAT(null);
        setFinalProf(null);
    }

    public static void StaticLogout(){
        if(getInstance().isLogin()) {
            getInstance().setFinalAT(null);
            getInstance().setFinalProf(null);
            LoginManager.getInstance().logOut();
        }
    }


    public void getUserData(Context mContext, GraphRequest.GraphJSONObjectCallback mCallBack){

        if (isLogin()) {
            GraphRequest request = GraphRequest.newMeRequest(FinalAT, mCallBack);
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,link,first_name, last_name, location, locale, timezone, verified");
            request.setParameters(parameters);
            request.executeAsync();
        }
        else{
            Toast.makeText(mContext,mContext.getString(R.string.ff_toast_at_empty),Toast.LENGTH_SHORT).show();
        }
    }


    public AccessTokenTracker getmATT() {
        return mATT;
    }

    public void setmATT(AccessTokenTracker mATT) {
        this.mATT = mATT;
    }

    public ProfileTracker getmPT() {
        return mPT;
    }

    public void setmPT(ProfileTracker mPT) {
        this.mPT = mPT;
    }

    public CallbackManager getmCallBackManager() {
        return mCallBackManager;
    }

    public void setmCallBackManager(CallbackManager mCallBackManager) {
        this.mCallBackManager = mCallBackManager;
    }

    public void setFinalAT(AccessToken finalAT) {
        FinalAT = finalAT;
    }

    public AccessToken getFinalAT() {
        return FinalAT;
    }

    public Profile getFinalProf() {
        return FinalProf;
    }

    public void setFinalProf(Profile finalProf) {
        FinalProf = finalProf;
    }
}
