package com.sgo.mdevcash.services;

/**
 * Created by Lenovo Thinkpad on 5/10/2016.
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
