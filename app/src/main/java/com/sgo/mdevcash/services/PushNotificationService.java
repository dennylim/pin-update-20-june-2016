package com.sgo.mdevcash.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.sgo.mdevcash.R;
import com.sgo.mdevcash.activities.CustomWebView;
import com.sgo.mdevcash.activities.DialogNotice;
import com.sgo.mdevcash.activities.GCMPlaystore;
import com.sgo.mdevcash.activities.MainPage;

import timber.log.Timber;

/**
 * Created by kundan on 10/22/2015.
 */
public class PushNotificationService extends GcmListenerService {
    public static int NOTIFICATION_ID = 1;
    TaskStackBuilder stackBuilder;
    boolean is_noty = false;
    Handler mHandler;
    private NotificationManager mNotificationManager;
    String temp = "1";

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {

        String message = data.getString("message");
        String title = data.getString("title");

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        Log.e("Denny", data.toString());
        sendNotification(data);
    }

    public class ShowMessage extends IntentService{

        /**
         * Creates an IntentService.  Invoked by your subclass's constructor.
         *
         * @param name Used to name the worker thread, important only for debugging.
         */

        Handler mHandler;
        public ShowMessage(String name) {
            super(name);
            mHandler = new Handler();
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            final String isi = intent.getStringExtra("isinya");

            Timber.e("masuk onHandleIntent isinya" + isi);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ShowMessage.this, isi, Toast.LENGTH_LONG).show();
                }
            });

            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void sendNotification(Bundle msg)
    {
        int num=++NOTIFICATION_ID;
        Log.e("BUNDLE", msg.toString());
        if (msg.getString("title").equals(null)) {
            msg.putString("title", "-");
        }
        if (msg.getString("message").equals(null)) {
            msg.putString("message", "-");
        }
        if (msg.getString("link").equals(null)) {
            msg.putString("link", "-");
        }
        if (msg.getString("emotion").equals(null)) {
            msg.putString("emotion", "-");
        }
        if(msg.getString("package").equals(null))
        {
            msg.putString("package", "-");
        }

        try
        {
            if(msg.getString("categories").equals(null))
            {
                msg.putString("categories", "-");
            }
            else
            {
                temp = msg.getString("categories");
                temp = temp.replaceAll("[^\\d.]", "");
            }
        }
        catch (Exception ex)
        {

        }

        Intent intent;
        Intent backIntent;
        PendingIntent pendingIntent = null;
        backIntent = new Intent(getApplicationContext(), MainPage.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if(temp.equals("2"))
        {
            switch (Integer.parseInt(msg.getString("type"))) {

                case 1:
                    break;
                case 2:
                    backIntent = new Intent(getApplicationContext(), DialogNotice.class);
                    backIntent.putExtras(msg);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case 3:
                    backIntent = new Intent(getApplicationContext(), CustomWebView.class);
                    backIntent.putExtras(msg);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case 4:
                    backIntent = new Intent(getApplicationContext(), GCMPlaystore.class);
                    backIntent.putExtras(msg);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case 5:
                    is_noty = true;
                    final String t = msg.getString("title");
                    mHandler.post(new DisplayToast(t));
                    break;

                case 6:
                    backIntent = new Intent(getApplicationContext(), MainPage.class);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (Integer.parseInt(msg.getString("type"))) {

                case 1:
                    break;
                case 2:
                    backIntent = new Intent(getApplicationContext(), DialogNotice.class);
                    backIntent.putExtras(msg);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case 3:
                    backIntent = new Intent(getApplicationContext(), CustomWebView.class);
                    backIntent.putExtras(msg);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case 4:
                    backIntent = new Intent(getApplicationContext(), GCMPlaystore.class);
                    backIntent.putExtras(msg);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case 5:
                    is_noty = true;
                    final String t = msg.getString("title");
                    mHandler.post(new DisplayToast(t));
                    break;

                case 6:
                    backIntent = new Intent(getApplicationContext(), MainPage.class);
                    stackBuilder = TaskStackBuilder.create(this);
                    stackBuilder.addNextIntent(backIntent);
                    pendingIntent = stackBuilder.getPendingIntent(num, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                default:
                    break;
            }
        }


        if (!is_noty) {
            mNotificationManager = (NotificationManager) this
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    this);

            mBuilder.setSmallIcon(R.mipmap.ic_launcher_pin_only)
                    .setContentTitle(msg.getString("title"))
                    .setStyle(
                            new NotificationCompat.BigTextStyle().bigText(msg
                                    .getString("message").toString()))
                    .setAutoCancel(true)
                    .setContentText(
                            msg.getString("title").concat(
                                    msg.getString("message")));

            if (Integer.parseInt(msg.getString("type")) != 1) {
                mBuilder.setContentIntent(pendingIntent);
            }

            mBuilder.setDefaults(Notification.DEFAULT_ALL);

            mNotificationManager.notify(++NOTIFICATION_ID, mBuilder.build());
        }
    }

    public class DisplayToast implements Runnable {
        String mText;

        public DisplayToast(String text) {
            Log.e("denny", text.toString());
            mText = text;
        }

        public void run() {
            Toast.makeText(PushNotificationService.this, mText, Toast.LENGTH_LONG)
                    .show();
        }
    }
}
