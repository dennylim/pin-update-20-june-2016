package com.sgo.mdevcash.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sgo.mdevcash.R;
import com.sgo.mdevcash.coreclass.DefineValue;
import com.sgo.mdevcash.coreclass.MyApiClient;
import com.sgo.mdevcash.coreclass.WebParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by Lenovo Thinkpad on 6/2/2016.
 */
public class GCMPlaystore extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    String package_name;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gcmcallplaystore);

        mProgressDialog=new ProgressDialog(GCMPlaystore.this);

        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        final Bundle b = getIntent().getExtras();
        package_name = b.getString("package");

        try {
            mProgressDialog.cancel();
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(myIntent);
            GCMPlaystore.this.finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);

//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
        } catch (android.content.ActivityNotFoundException anfe) {
            mProgressDialog.cancel();
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + package_name));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(myIntent);
            GCMPlaystore.this.finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + package_name)));
        }


    }

    public void onBackPressed() {
            super.onBackPressed();
            GCMPlaystore.this.finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
    }
}
