package com.sgo.mdevcash.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sgo.mdevcash.R;

/**
 * Created by Lenovo Thinkpad on 5/18/2016.
 */
public class DialogNotice extends AppCompatActivity{
    TextView date,code,msg,flink,title;
    Button readmore,close;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gcm_dialog);

        date=(TextView)findViewById(R.id.date);
        code=(TextView)findViewById(R.id.code);
        msg=(TextView)findViewById(R.id.msg);
        readmore=(Button)findViewById(R.id.readmore);

        final Bundle b=getIntent().getExtras();

        date.setText(b.getString("title"));
        code.setText(b.getString("emotion"));
        msg.setText(b.getString("message"));
        Linkify.addLinks(msg, Linkify.ALL);

        if(b.getString("link").equalsIgnoreCase("http://")){
            readmore.setVisibility(View.GONE);
        }else{
            readmore.setVisibility(View.VISIBLE);
            readmore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(getApplicationContext(), CustomWebView.class);

                    intent.putExtra("link", b.getString("link"));
                    startActivity(intent);
                }
            });

        }

        close=(Button)findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }
}
