package com.sgo.mdevcash.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.securepreferences.SecurePreferences;
import com.sgo.mdevcash.Beans.BalanceModel;
import com.sgo.mdevcash.Beans.navdrawmainmenuModel;
import com.sgo.mdevcash.R;
import com.sgo.mdevcash.activities.LevelFormRegisterActivity;
import com.sgo.mdevcash.activities.MainPage;
import com.sgo.mdevcash.activities.MyProfileActivity;
import com.sgo.mdevcash.adapter.NavDrawMainMenuAdapter;
import com.sgo.mdevcash.coreclass.CurrencyFormat;
import com.sgo.mdevcash.coreclass.CustomSecurePref;
import com.sgo.mdevcash.coreclass.DefineValue;
import com.sgo.mdevcash.coreclass.MyApiClient;
import com.sgo.mdevcash.coreclass.MyPicasso;
import com.sgo.mdevcash.coreclass.RoundImageTransformation;
import com.sgo.mdevcash.coreclass.WebParams;
import com.sgo.mdevcash.dialogs.AlertDialogFrag;
import com.sgo.mdevcash.dialogs.MessageDialog;
import com.sgo.mdevcash.dialogs.ReportBillerDialog;
import com.sgo.mdevcash.interfaces.OnLoadDataListener;
import com.sgo.mdevcash.loader.UtilsLoader;
import com.sgo.mdevcash.services.BalanceService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import timber.log.Timber;

/*
  Created by Administrator on 12/8/2014.
 */
public class NavigationDrawMenu extends ListFragment{

    public static final int MPAYFRIENDS = 2;
    public static final int MASK4MONEY = 3;

    private ImageView headerCustImage;
    private TextView headerCustName,headerCustID,headerCurrency,balanceValue, currencyLimit, limitValue,periodeLimit;

    private Animation frameAnimation;
    private ImageView btn_refresh_balance;

    ListView mListView;
    private View v;
    public NavDrawMainMenuAdapter mAdapter;
    Boolean isLevel1,isRegisteredLevel,isAllowedLevel;
    Bundle _SaveInstance;
    SecurePreferences sp;
    String contactCenter,listContactPhone = "", listAddress="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_navigation_draw_menu_main, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _SaveInstance = savedInstanceState;

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        mAdapter = new NavDrawMainMenuAdapter(getActivity(), generateData());
        mListView = (ListView) v.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        LinearLayout llHeaderProfile = (LinearLayout) v.findViewById(R.id.llHeaderProfile);
        headerCustImage = (ImageView) v.findViewById(R.id.header_cust_image);
        headerCurrency = (TextView) v.findViewById(R.id.currency_value);
        headerCustName = (TextView) v.findViewById(R.id.header_cust_name);
        headerCustID = (TextView) v.findViewById(R.id.header_cust_id);
        balanceValue = (TextView) v.findViewById(R.id.balance_value);
        currencyLimit = (TextView) v.findViewById(R.id.currency_limit_value);
        limitValue = (TextView) v.findViewById(R.id.limit_value);
        periodeLimit = (TextView) v.findViewById(R.id.periode_limit_value);

        refreshUINavDrawer();
        refreshDataNavDrawer();

        llHeaderProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MyProfileActivity.class);
                switchActivity(i, MainPage.ACTIVITY_RESULT);
            }
        });


        btn_refresh_balance = (ImageView) v.findViewById(R.id.btn_refresh_balance);
        frameAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.spinner_animation);
        frameAnimation.setRepeatCount(Animation.INFINITE);

        btn_refresh_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_refresh_balance.setEnabled(false);
                btn_refresh_balance.startAnimation(frameAnimation);
                getBalance();
            }
        });

        BalanceModel mBal = BalanceModel.load(BalanceModel.class,1);
        if(mBal != null)
            setBalanceToUI(mBal);

    }

    public void setBalanceToUI(BalanceModel deData){
        headerCurrency.setText(deData.getCcy_id());
        balanceValue.setText(CurrencyFormat.format(deData.getAmount()));
        currencyLimit.setText(deData.getCcy_id());
        limitValue.setText(CurrencyFormat.format(deData.getRemain_limit()));

        if (deData.getPeriod_limit().equals("Monthly"))
            periodeLimit.setText(R.string.header_monthly_limit);
        else
            periodeLimit.setText(R.string.header_daily_limit);
    }

    public void getBalance(){
        new UtilsLoader(getActivity(),sp).getDataBalance(new OnLoadDataListener() {
            @Override
            public void onSuccess(Object deData) {
                setBalanceToUI((BalanceModel) deData);
                btn_refresh_balance.setEnabled(true);
                btn_refresh_balance.clearAnimation();

                Intent i = new Intent(BalanceService.INTENT_ACTION_BALANCE);
                i.putExtra(BalanceModel.BALANCE_PARCELABLE, (BalanceModel) deData);
                LocalBroadcastManager.getInstance(getActivity())
                        .sendBroadcast(i);
            }

            @Override
            public void onFail(String message) {

                btn_refresh_balance.setEnabled(true);
                btn_refresh_balance.clearAnimation();
            }

            @Override
            public void onFailure() {

                btn_refresh_balance.setEnabled(true);
                btn_refresh_balance.clearAnimation();

            }
        });
    }

    private void setImageProfPic(){
        float density = getResources().getDisplayMetrics().density;
        String _url_profpic;

        if(density <= 1) _url_profpic = sp.getString(DefineValue.IMG_SMALL_URL, null);
        else if(density < 2) _url_profpic = sp.getString(DefineValue.IMG_MEDIUM_URL, null);
        else _url_profpic = sp.getString(DefineValue.IMG_LARGE_URL, null);

        Timber.wtf("url prof pic:" + _url_profpic);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.user_unknown_menu);
        RoundImageTransformation roundedImage = new RoundImageTransformation(bm);

        Picasso mPic;
        if(MyApiClient.PROD_FLAG_ADDRESS)
            mPic = MyPicasso.getImageLoader(getActivity());
        else
            mPic= Picasso.with(getActivity());

        if(_url_profpic !=null && _url_profpic.isEmpty()){
            mPic.load(R.drawable.user_unknown_menu)
                    .error(roundedImage)
                    .fit().centerInside()
                    .placeholder(R.anim.progress_animation)
                    .transform(new RoundImageTransformation()).into(headerCustImage);
        }
        else {
            mPic.load(_url_profpic)
                    .error(roundedImage)
                    .fit().centerInside()
                    .placeholder(R.anim.progress_animation)
                    .transform(new RoundImageTransformation()).into(headerCustImage);
        }

    }

    public void initializeNavDrawer(){
        Fragment newFragment = new FragMainPage();
        switchFragment(newFragment, getString(R.string.toolbar_title_home));

        refreshDataNavDrawer();

        contactCenter = sp.getString(DefineValue.LIST_CONTACT_CENTER, "");

        try {
            JSONArray arrayContact = new JSONArray(contactCenter);
            for(int i=0 ; i<arrayContact.length() ; i++) {
//                String contactPhone = arrayContact.getJSONObject(i).getString(WebParams.CONTACT_PHONE);
//                if(i == arrayContact.length()-1) {
//                    listContactPhone += contactPhone;
//                }
//                else {
//                    listContactPhone += contactPhone + " atau ";
//                }

                if(i == 0) {
                    listContactPhone = arrayContact.getJSONObject(i).getString(WebParams.CONTACT_PHONE);
                    listAddress = arrayContact.getJSONObject(i).getString(WebParams.ADDRESS);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void refreshUINavDrawer(){
        setImageProfPic();
        headerCustName.setText(sp.getString(DefineValue.CUST_NAME, getString(R.string.text_strip)));
        headerCustID.setText(sp.getString(DefineValue.CUST_ID, getString(R.string.text_strip)));
    }
    public void refreshDataNavDrawer(){
        if(sp.contains(DefineValue.LEVEL_VALUE)) {
            int i = sp.getInt(DefineValue.LEVEL_VALUE, 0);
            isLevel1 = i == 1;
            isRegisteredLevel = sp.getBoolean(DefineValue.IS_REGISTERED_LEVEL, false);
            isAllowedLevel = sp.getBoolean(DefineValue.ALLOW_MEMBER_LEVEL, false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        selectItem(position,null);
    }

    private ArrayList<navdrawmainmenuModel> generateData(){
        ArrayList<navdrawmainmenuModel> models = new ArrayList<navdrawmainmenuModel>();
        models.add(new navdrawmainmenuModel(0,0,getString(R.string.menu_group_title_main_menu),true));                                        //0
//        models.add(new navdrawmainmenuModel(R.drawable.ic_home_icon_color,getString(R.string.menu_item_title_home),false));              //
//        models.add(new navdrawmainmenuModel(R.drawable.ic_accounts_icon_color,getString(R.string.menu_item_title_accounts),false));        //
        models.add(new navdrawmainmenuModel(R.drawable.ic_topup_icon_white,R.drawable.ic_topup_icon_color,getString(R.string.menu_item_title_topup),false));              //1
        models.add(new navdrawmainmenuModel(R.drawable.ic_payfriends_icon_white,R.drawable.ic_payfriends_icon_color,getString(R.string.menu_item_title_pay_friends),false));    //2
        models.add(new navdrawmainmenuModel(R.drawable.ic_ask_icon_white,R.drawable.ic_ask_icon_color,getString(R.string.menu_item_title_ask_for_money),false));            //3
        models.add(new navdrawmainmenuModel(R.drawable.ic_buy_icon_white,R.drawable.ic_buy_icon_color,getString(R.string.menu_item_title_buy),false));             //4
        models.add(new navdrawmainmenuModel(R.drawable.ic_cashout_icon_white,R.drawable.ic_cashout_icon_color,getString(R.string.menu_item_title_cash_out),false));       //5


        models.add(new navdrawmainmenuModel(0,0,getString(R.string.menu_group_title_account),true));                                         //7
        models.add(new navdrawmainmenuModel(R.drawable.ic_friends_icon_white,R.drawable.ic_friends_icon_color,getString(R.string.menu_item_title_my_friends),false));        //8
//        models.add(new navdrawmainmenuModel(R.drawable.ic_groups_icon_white,R.drawable.ic_groups_icon_color,getString(R.string.menu_item_title_my_groups),false));          //9

        models.add(new navdrawmainmenuModel(0,0,getString(R.string.menu_group_title_supports),true));                                        //10
        models.add(new navdrawmainmenuModel(R.drawable.ic_report_white,R.drawable.ic_report_color,getString(R.string.menu_item_title_report),false));              //6
        models.add(new navdrawmainmenuModel(R.drawable.ic_setting_white,R.drawable.ic_setting_color,getString(R.string.menu_item_title_setting),false));                    //11
        models.add(new navdrawmainmenuModel(R.drawable.ic_help_white,R.drawable.ic_help_color,getString(R.string.menu_item_title_help),false));                          //12
        models.add(new navdrawmainmenuModel(0,0,getString(R.string.menu_group_title_logout),true));                                        //13
        models.add(new navdrawmainmenuModel(R.drawable.ic_logout_icon_white,R.drawable.ic_logout_icon_color,getString(R.string.menu_item_title_logout),false));                 //14

        return models;
    }

    public void selectItem(int position, Bundle data){
        Fragment newFragment;
        Intent newIntent;
        mAdapter.setSelectedItem(position);
        mAdapter.notifyDataSetChanged();
        switch (position) {
            case 1:
                newFragment = new ListTopUp();
                switchFragment(newFragment, getString(R.string.toolbar_title_topup));
                break;
            case MPAYFRIENDS:
                if(isAllowedLevel && isLevel1) {
                    if(isRegisteredLevel)
                        showDialogLevelRegistered();
                    else
                        showDialogLevel();
                }
                else {
                    newFragment = new FragPayFriends();
                    if (data != null && !data.isEmpty()) newFragment.setArguments(data);
                    switchFragment(newFragment, getString(R.string.menu_item_title_pay_friends));
                }
                break;
            case MASK4MONEY:
                if(isAllowedLevel && isLevel1) {
                    if (isRegisteredLevel)
                        showDialogLevelRegistered();
                    else
                        showDialogLevel();
                }else {
                    newFragment = new FragAskForMoney();
                    if (data != null && !data.isEmpty()) newFragment.setArguments(data);
                    switchFragment(newFragment, getString(R.string.menu_item_title_ask_for_money));
                }
                break;
            case 4:
                newFragment = new ListBuy();
                switchFragment(newFragment, getString(R.string.toolbar_title_purchase));
                break;
            case 5:
                if(isAllowedLevel && isLevel1) {
                    if(isRegisteredLevel)
                        showDialogLevelRegistered();
                    else
                        showDialogLevel();
                }else {
                    newFragment = new ListCashOut();
                    switchFragment(newFragment, getString(R.string.menu_item_title_cash_out));
                }
                break;

            case 7:
                newFragment = new ListMyFriends();
                switchFragment(newFragment, getString(R.string.toolbar_title_myfriends));
                break;
//            case 8:
//                newFragment = new FragMyGroup();
//                switchFragment(newFragment, getString(R.string.toolbar_title_mygroup));
//                break;
            case 9:
                newFragment = new ReportTab();
                switchFragment(newFragment, getString(R.string.menu_item_title_report));
                break;
            case 10:
                newFragment = new ListSettings();
                switchFragment(newFragment, getString(R.string.menu_item_title_setting));
                break;
            case 11:
                newFragment = new ContactTab();
                switchFragment(newFragment, getString(R.string.menu_item_title_help));
                break;
            case 13:
                AlertDialog.Builder alertbox=new AlertDialog.Builder(getActivity());
                alertbox.setTitle(getString(R.string.warning));
                alertbox.setMessage(getString(R.string.exit_message));
                alertbox.setPositiveButton(getString(R.string.ok), new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                switchLogout();
                            }
                        });
                alertbox.setNegativeButton(getString(R.string.cancel), new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {}
                        });
                alertbox.show();
                break;
        }
    }

    private void showDialogLevel(){
        final AlertDialogFrag dialog_frag = AlertDialogFrag.newInstance(getString(R.string.level_dialog_title),
                getString(R.string.level_dialog_message),getString(R.string.level_dialog_btn_ok),getString(R.string.cancel),false);
        dialog_frag.setOkListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent mI = new Intent(getActivity(), LevelFormRegisterActivity.class);
                switchActivity(mI, MainPage.ACTIVITY_RESULT);
            }
        });
        dialog_frag.setCancelListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog_frag.dismiss();
            }
        });
        dialog_frag.setTargetFragment(NavigationDrawMenu.this, 0);
//        dialog_frag.show(getActivity().getSupportFragmentManager(), AlertDialogFrag.TAG);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(dialog_frag, null);
        ft.commitAllowingStateLoss();
    }

    private void showDialogLevelRegistered(){
        MessageDialog dialognya;
        dialognya = new MessageDialog(getActivity(), getString(R.string.level_dialog_finish_title),
                getString(R.string.level_dialog_finish_message) + "\n" + listAddress + "\n" +
                getString(R.string.level_dialog_finish_message_2) + "\n" + listContactPhone);

        dialognya.setDialogButtonClickListener(new MessageDialog.DialogButtonListener() {
            @Override
            public void onClickButton(View v, boolean isLongClick) {

            }
        });
        dialognya.show();
    }

    private void showMyCustomDialog() {
        ReportBillerDialog dialog = new ReportBillerDialog();
        dialog.show(getActivity().getSupportFragmentManager(), "asfasfaf");
    }

    private void switchFragment(Fragment i, String name){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchContent(i,name);
    }

    private void switchLogout(){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchLogout();
    }


    private void switchActivity(Intent mIntent,int j){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,j);
    }

    public void setPositionNull(){
        mAdapter.setDefault();
        mAdapter.notifyDataSetChanged();
    }

}